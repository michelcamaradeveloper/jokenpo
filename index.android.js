/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

 //Importando componentes
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native';

//Importando componentes modularizados
/* Modularizar ajuda a diminuir o tamanho de codigos em um arquivo e tambem na futura manutenção dele */
import Topo from './src/components/topo';
import Icone from './src/components/icone';

//Classe principal do aplicativo
export default class app3 extends Component {
  //Constructor e state para deixar o app dinamico
  constructor(props){
    super(props);

    //iniciando os states com vazio
    this.state={ escolhaUsuario:'', 
                  escolhaComputador:'', 
                  resultado:''
    }
  }

  //Função que determina os resultados do jogo
  //Recebe como parametro a escolha que o usuário fez ao clicar no botão
  jokenpo(escolhaUsuario){
    //Gerar numero aleatorio de 0-2
    var numAleatorio = Math.floor(Math.random()*3);

    //Criando variavel vazia
    var escolhaComputador ='';
    //Switch recebe por parametro um numero de 0-2 para determinar a ''escolha'' do computador
    switch(numAleatorio){
      case 0: escolhaComputador ='Pedra'; break;
      case 1 : escolhaComputador ='Papel'; break;
      case 2 : escolhaComputador ='Tesoura'; break;
    }

    
    //criando variavel vazia
    var resultado = '';
    //Determinando resultado se a escolha do computador for Pedra
    if(escolhaComputador == 'Pedra'){
      if(escolhaUsuario == 'Pedra'){
        resultado = 'Empate!';
      }
      if(escolhaUsuario == 'Papel'){
        resultado = 'Você Ganhou!';
      }
      if(escolhaUsuario == 'Tesoura'){
        resultado = 'Você Perdeu!';
      }
    }

    //Determinando resultado se a escolha do computador for Papel
    if(escolhaComputador == 'Papel'){
      if(escolhaUsuario == 'Pedra'){
        resultado = 'Você Perdeu!';
      }
      if(escolhaUsuario == 'Papel'){
        resultado = 'Empate!';
      }
      if (escolhaUsuario == 'Tesoura'){
        resultado = 'Você Ganhou!';
      }
    }
    //Determinando resultado se a escolha do computador for Tesoura
    if(escolhaComputador == 'Tesoura'){
      if(escolhaUsuario == 'Pedra'){
        resultado = 'Você Ganhou!';
      }
      if(escolhaUsuario == 'Papel'){
        resultado ='Você Perdeu!';
      }
      if(escolhaUsuario == 'Tesoura'){
        resultado = 'Empate!';
      }
    }

    //Faz a mudança de estado das states que foram criadas vazias
    this.setState({escolhaUsuario : escolhaUsuario,
      escolhaComputador : escolhaComputador,
      resultado : resultado
    });
  }

  //Função renderizar os componentes na tela
  //Mostra os componentes na tela
  render() {
    return (
      <View>
        {/* Chamando componente renderizado Topo */}
        {/* Nele contem a imagem apresentada no topo do app */}
        {/* Modularizar ajuda a diminuir o tamanho de codigos em um arquivo e tambem na futura manutenção dele */}
        <Topo></Topo>

        {/* Divisão da tela que contem os botões, cada botão contem a função onPress que envia a escolha do usuário */}
        <View style={styles.painelAcoes}>
          <View style={styles.btnEscolha}>
            <Button
              title='Pedra'
              onPress={()=>{this.jokenpo('Pedra')}}
            />
          </View>
          <View style={styles.btnEscolha}>
            <Button
              title='Papel'
              onPress={()=>{this.jokenpo('Papel')}}
            />
          </View>
          <View style={styles.btnEscolha}>
            <Button
              title='Tesoura'
              onPress={()=>{this.jokenpo('Tesoura')}}
            />
          </View>
        </View>

        {/* Divisão da tela que apresenta o resultado, a escolha do usuário e do computador */}
        <View style={styles.palco}>
          <Text style={styles.txtResultado}>{this.state.resultado}</Text>

          {/* Chamando componente renderizado Icone */}
          {/* Nele contem a parte de apresentação dos resultados, parte visual */}
          {/* Modularizar ajuda a diminuir o tamanho de codigos em um arquivo e tambem na futura manutenção dele */}
          {/* Passando via props a escolha do computador e usuário, e o tipo do jogador */}
          <Icone escolha={this.state.escolhaComputador} jogador='Computador'></Icone>
          <Icone escolha={this.state.escolhaUsuario} jogador='Você'></Icone>
          
        </View>
      
      </View>
    );
  }
}


// Criação dos estilos, similar a CSS3
const styles = StyleSheet.create({
  btnEscolha:{
    width: 100
  }, 
  painelAcoes:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10
  },
  palco:{
    alignItems: 'center',
    marginTop: 20
  },
  txtResultado:{
    fontSize: 30,
    fontWeight: 'bold',
    color: 'red',
    height: 60
  },
  icone:{
    alignItems:'center',
    marginBottom: 20
  },
  txtJogador:{
    fontSize:25
  }
});

//Registro da classe que irá ser apresentada como padrão
//Podemos até dizer que aqui informa a classe inicial de apresentação do app
AppRegistry.registerComponent('app3', () => app3);